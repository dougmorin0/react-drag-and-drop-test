import React from 'react';
import './App.css';
import Sortable from 'react-sortablejs';

class App extends React.Component {

  componentDidUpdate() {
    console.log(this.state);
  }

  state = {
    services: [{"id":5,"name":"Accounts"},{"id":2,"name":"Content"},{"id":3,"name":"Creative"},{"id":4,"name":"Digital"},{"id":6,"name":"Media"},{"id":1,"name":"Morale"},{"id":8,"name":"Operations"},{"id":7,"name":"PR"},{"id":9,"name":"Strategy"}],
  }

  render() {

    const Services = this.state.services.map(itm => (<li className="sort-li" key={itm.id} data-id={itm.id} data-name={itm.name}>{itm.name}</li>));

    return (
      <div>
        <Sortable
          tag="ul"
          onChange={(order, sortable, evt) => {
            let newServices = [];

            for (let i in order) {
              const matchedService = this.state.services.filter(service => {
                return parseInt(service.id) === parseInt(order[i]);
              });
              newServices.push(matchedService[0]);
            }
            this.setState({ services: newServices });
          }}
        >
          {Services}
        </Sortable>
      </div>
    );
  }
}

export default App;
